package edu.tcu.mi.ihe.fhir.model;

import ca.uhn.fhir.model.api.annotation.ResourceDef;
import ca.uhn.fhir.model.dstu2.resource.DocumentManifest;

@ResourceDef(name="DocumentManifest")
public class IHEDocumentManifest extends DocumentManifest {

}
